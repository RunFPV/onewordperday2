/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect, useState} from 'react';
import {Appearance, StatusBar} from 'react-native';
import AppNavigator from './src/AppNavigator';
import {ApplicationProvider} from '@ui-kitten/components';
import * as eva from '@eva-design/eva';
import {appTheme} from './src/themes';

const App = () => {
  const [darkMode, setdarkmode] = useState(false);

  useEffect(() => {
    setdarkmode(Appearance.getColorScheme() === 'dark');
  }, []);

  return (
    <ApplicationProvider
      {...eva}
      theme={darkMode ? appTheme.darkTheme : appTheme.lightTheme}>
      <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} />
      <AppNavigator darkMode={darkMode} />
    </ApplicationProvider>
  );
};

export default App;
