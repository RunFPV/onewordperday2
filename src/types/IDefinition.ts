interface IDefinition {
  nature?: string;
  definition: string;
}

export default IDefinition;
