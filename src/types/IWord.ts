import IDefinition from './IDefinition';
import IExpression from './IExpression';
import IQuote from './IQuote';

interface IWord {
  mot: string;
  scorescrabble: number;
  definitions: IDefinition[];
  citations?: IQuote[];
  expressions?: IExpression[];
}

export default IWord;
