import http from './http-common';
import IWord from '../types/IWord';
import {KEY} from './config';

const getDayWord = (date: string) => {
  return http.get<IWord>(`/mots/motdujour?date=${date}&api_key=${KEY}`);
};

const WordService = {
  getDayWord,
};
export default WordService;
