import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {navTheme} from './themes';

import * as s from './screens';

const Stack = createNativeStackNavigator();

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="One Word per Day" component={s.MainScreen} />
    </Stack.Navigator>
  );
};

const AppNavigator = ({darkMode}: {darkMode: boolean}) => {
  return (
    <NavigationContainer
      theme={darkMode ? navTheme.darkTheme : navTheme.lightTheme}>
      <HomeStackNavigator />
    </NavigationContainer>
  );
};

export default AppNavigator;
