import React from 'react';
import {Layout, withStyles, Spinner} from '@ui-kitten/components';

const LoadingComponent = ({eva: {style}}: {eva: any}) => {
  return (
    <Layout style={style.container}>
      <Spinner size="large" />
    </Layout>
  );
};

const styles = () => ({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Loading = withStyles(LoadingComponent, styles);
export default Loading;
