import React from 'react';
import {Layout, withStyles} from '@ui-kitten/components';
import IDefinition from '../../types/IDefinition';
import Item from './ListItem';

const DefinitionListComponent = (props: any) => {
  const {
    eva: {style},
    definitions,
  }: {
    eva: any;
    definitions: Array<IDefinition> | undefined;
  } = props;

  const printDefinitions = () => {
    if (!definitions) {
      return;
    }
    return definitions.map((d, index) => {
      return (
        <Item
          key={index}
          data={d.definition}
          prefix={d.nature}
          eva={undefined}
          isQuote={false}
        />
      );
    });
  };

  return <Layout style={style.container}>{printDefinitions()}</Layout>;
};

const styles = (theme: any) => ({
  container: {
    paddingVertical: theme['spacing-s'],
  },
});

const DefinitionList = withStyles(DefinitionListComponent, styles);
export default DefinitionList;
