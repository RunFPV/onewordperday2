import React from 'react';
import {Layout, Text, withStyles} from '@ui-kitten/components';
import Item from './ListItem';
import IExpression from '../../types/IExpression';

const ExpressionListComponent = (props: any) => {
  const {
    eva: {style},
    expressions,
  }: {
    eva: any;
    expressions: Array<IExpression> | undefined;
  } = props;

  const printExpressions = () => {
    if (!expressions) {
      return;
    }
    return expressions.map((e, index) => {
      return (
        <Item key={index} data={e.expression} eva={undefined} isQuote={false} />
      );
    });
  };

  return (
    <Layout style={style.container}>
      <Text style={style.title} category="h4">
        • Expressions •
      </Text>
      {printExpressions()}
    </Layout>
  );
};

const styles = (theme: any) => ({
  container: {
    paddingVertical: theme['spacing-s'],
  },
  title: {
    textAlign: 'center',
  },
});

const ExpressionList = withStyles(ExpressionListComponent, styles);
export default ExpressionList;
