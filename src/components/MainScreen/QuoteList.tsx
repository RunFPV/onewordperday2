import React from 'react';
import {Layout, Text, withStyles} from '@ui-kitten/components';
import IQuote from '../../types/IQuote';
import Item from './ListItem';

const QuoteListComponent = (props: any) => {
  const {
    eva: {style},
    quotes,
  }: {
    eva: any;
    quotes: Array<IQuote> | undefined;
  } = props;

  const printQuotes = () => {
    if (!quotes) {
      return;
    }
    return quotes.map((q, index) => {
      return <Item key={index} data={q.citation} isQuote eva={undefined} />;
    });
  };

  return (
    <Layout style={style.container}>
      <Text style={style.title} category="h4">
        • Citations •
      </Text>
      {printQuotes()}
    </Layout>
  );
};

const styles = (theme: any) => ({
  container: {
    paddingVertical: theme['spacing-s'],
  },
  title: {
    textAlign: 'center',
  },
});

const QuoteList = withStyles(QuoteListComponent, styles);
export default QuoteList;
