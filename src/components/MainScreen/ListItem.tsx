import React from 'react';
import {Layout, Text, withStyles} from '@ui-kitten/components';

const ListItemComponent = (props: any) => {
  const {
    eva: {style},
    prefix,
    data,
    isQuote,
  }: {
    eva: any;
    prefix?: string;
    data: string;
    isQuote: boolean;
  } = props;

  return (
    <Layout style={style.container}>
      <Text>
        {prefix ? <Text style={style.prefix}>{prefix} : </Text> : ''}
        {isQuote ? (
          <Text style={style.quote}>"{data}"</Text>
        ) : (
          <Text style={isQuote && style.quote}>{data}</Text>
        )}
      </Text>
    </Layout>
  );
};

const styles = (theme: any) => ({
  container: {
    paddingVertical: theme['spacing-xs'],
  },
  prefix: {
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  quote: {
    fontStyle: 'italic',
  },
});

const ListItem = withStyles(ListItemComponent, styles);
export default ListItem;
