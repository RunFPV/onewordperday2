import {DarkTheme, DefaultTheme} from '@react-navigation/native';
import {colors} from './appTheme';

const darkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    // background: dark['background-basic-color-1'],
    // card: dark['background-basic-color-1'],
    background: colors.DARK_BACKGROUND,
    card: colors.DARK_BACKGROUND,
  },
};

const lightTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: colors.LIGHT_BACKGROUND,
    card: colors.LIGHT_BACKGROUND,
  },
};

export {darkTheme, lightTheme};
