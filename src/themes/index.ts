import * as navTheme from './navTheme';
import * as appTheme from './appTheme';

export {appTheme, navTheme};
