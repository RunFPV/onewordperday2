import {light, dark} from '@eva-design/eva';

const colors = {
  DARK_BACKGROUND: '#192038',
  LIGHT_BACKGROUND: '#f7f9fc',
};

const shared = {
  'color-primary-100': '#D3FEF1',
  'color-primary-200': '#A7FDEA',
  'color-primary-300': '#7AFAE8',
  'color-primary-400': '#59F5ED',
  'color-primary-500': '#24E8EF',
  'color-primary-600': '#1AB8CD',
  'color-primary-700': '#128CAC',
  'color-primary-800': '#0B658A',
  'color-primary-900': '#064A72',
  'color-success-100': '#EAFBD3',
  'color-success-200': '#D1F8A8',
  'color-success-300': '#ADEC7A',
  'color-success-400': '#88D956',
  'color-success-500': '#57C126',
  'color-success-600': '#3EA51B',
  'color-success-700': '#298A13',
  'color-success-800': '#186F0C',
  'color-success-900': '#0B5C07',
  'color-info-100': '#D2EFFE',
  'color-info-200': '#A5DBFE',
  'color-info-300': '#79C3FE',
  'color-info-400': '#57ACFD',
  'color-info-500': '#2087FC',
  'color-info-600': '#1768D8',
  'color-info-700': '#104DB5',
  'color-info-800': '#0A3692',
  'color-info-900': '#062678',
  'color-warning-100': '#FFF9D0',
  'color-warning-200': '#FFF0A1',
  'color-warning-300': '#FFE672',
  'color-warning-400': '#FFDC4E',
  'color-warning-500': '#FFCC14',
  'color-warning-600': '#DBAA0E',
  'color-warning-700': '#B78A0A',
  'color-warning-800': '#936B06',
  'color-warning-900': '#7A5503',
  'color-danger-100': '#FFDFD8',
  'color-danger-200': '#FFB8B1',
  'color-danger-300': '#FF8A8B',
  'color-danger-400': '#FF6D7C',
  'color-danger-500': '#FF3D63',
  'color-danger-600': '#DB2C5F',
  'color-danger-700': '#B71E59',
  'color-danger-800': '#931351',
  'color-danger-900': '#7A0B4B',

  // Font Weight
  'font-thin': '100',
  'font-ultra-light': '200',
  'font-light': '300',
  'font-regular': '400',
  'font-medium': '500',
  'font-semi-bold': '600',
  'font-bold': '700',

  // Font size
  'font-size-extra-large': 20,
  'font-size-large': 18,
  'font-size-medium': 16,
  'font-size-small': 14,
  'font-size-extra-small': 12,
  'font-size-extra-extra-small': 10,

  // Spacings
  'spacing-xs': 3,
  'spacing-s': 5,
  'spacing-m': 10,
  'spacing-l': 20,
};

const lightTheme = {
  ...light,
  ...shared,
};

const darkTheme = {
  ...dark,
  ...shared,
};

export {lightTheme, darkTheme, colors};
