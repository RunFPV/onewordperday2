import React, {useEffect, useState} from 'react';
import {Layout, Text, withStyles} from '@ui-kitten/components';
import styles from './MainScreen.style';
import IWord from '../../types/IWord';
import WordService from '../../services/wordService';
import DefinitionList from '../../components/MainScreen/DefinitionList';
import QuoteList from '../../components/MainScreen/QuoteList';
import ExpressionList from '../../components/MainScreen/ExpressionList';
import Loading from '../../components/Shared/Loading';
import {RefreshControl, ScrollView} from 'react-native';

const MainScreenComponent = ({eva: {style}}: {eva: any}) => {
  const [word, setword] = useState<IWord>();
  const [loading, setloading] = useState(true);

  useEffect(() => {
    retrieveWord();
  }, []);

  const capitalizeFirstLetter = (str?: string) => {
    if (!str) {
      return '';
    }
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  const retrieveWord = () => {
    setloading(true);
    WordService.getDayWord(new Date().toISOString().slice(0, 10))
      .then((response: any) => setword(response.data))
      .catch((e: Error) => console.log(e))
      .finally(() => setloading(false));
  };

  if (!word) {
    return <Loading eva={undefined} />;
  }

  return (
    <ScrollView
      contentContainerStyle={style.scrollContainer}
      refreshControl={
        <RefreshControl refreshing={loading} onRefresh={retrieveWord} />
      }>
      <Layout style={style.container}>
        <Layout style={style.titleContainer}>
          <Text style={style.title} category="h1">
            {capitalizeFirstLetter(word?.mot)}
          </Text>
          {word && (
            <Text style={style.subTitle}>
              Score au scrabble : {word.scorescrabble}
            </Text>
          )}
        </Layout>
        <DefinitionList definitions={word?.definitions} eva={undefined} />

        {word?.citations && word?.citations.length > 0 && (
          <QuoteList quotes={word.citations} eva={undefined} />
        )}

        {word?.expressions && word?.expressions.length > 0 && (
          <ExpressionList expressions={word.expressions} eva={undefined} />
        )}
      </Layout>
    </ScrollView>
  );
};

const MainScreen = withStyles(MainScreenComponent, styles);
export default MainScreen;
