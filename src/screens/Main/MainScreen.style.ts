const styles = (theme: any) => ({
  scrollContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: theme['spacing-m'],
  },
  titleContainer: {
    padding: theme['spacing-l'],
  },
  title: {
    textAlign: 'center',
    paddingBot: theme['spacing-m'],
  },
  subTitle: {
    textAlign: 'center',
  },
});

export default styles;
